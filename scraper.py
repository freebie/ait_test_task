import requests
import re
import json
from bs4 import BeautifulSoup

url = 'https://www.yellowpages.com/austin-tx/plumbers'
page_arg = '?page={0}'


def scrap_data():
    global url
    global page_arg

    pages: list = list()
    current_page: int = 1
    while True:
        current_url = url + page_arg.format(current_page)
        html = requests.get(current_url)

        soup = BeautifulSoup(html.text, 'lxml')
        pages.append(soup)
        if soup.find('a', {'class': 'next ajax-page'}):
            current_page += 1
        else:
            break

    parse_data_to_dict(pages)


def parse_data_to_dict(pages):
    res: dict = dict()
    for soup in pages:
        all = soup.find_all('div', {"class": 'v-card'})
        for company in all:
            try:
                links = company.find('div', 'links')
                links = [a.text for a in links.find_all('a')]
                if 'Directions' in links and re.match(r'[0-9]{0,9}\.', company.find('h2', {"class": "n"}).text):
                    name = re.sub(r'[0-9]{0,9}\. ', '', company.find('h2', {"class": "n"}).text)
                    adr = company.find('div', {'class': 'street-address'}).text +\
                          company.find('div', {'class': 'locality'}).text
                    phone = re.sub(r'[^0-9]', '', company.find('div', {'class': 'phones phone primary'}).text)

                    res[name] = {
                        'Address': adr,
                        'Phone Number': phone
                    }

            except Exception as e:
                print(e)

    data_to_json(res)


def data_to_json(data: dict):
    with open('plumbing.json', 'w') as outfile:
        json.dump(data, outfile)
